A reconstruction of Reichard's cubic globe as [described by Furuti](http://www.progonos.com/furuti/MapProj/Normal/ProjPoly/projPoly2.html).

See also [Earth in a Cube II](http://bl.ocks.org/espinielli/0c130de06ee3c01c0a63ba9ce06bc7bd).

This is D3v3 with the addition of a `polyhedron.js` from
[Jason Davies](https://www.jasondavies.com/maps/) that I originally used in
[Cahill-Keyes](http://bl.ocks.org/espinielli/b6e2f37814a19dc461f8)' map projection.
This library unfortunatelly never made it officially into D3 geo...

Forked from <a href='http://bl.ocks.org/espinielli/'>espinielli</a>'s block: <a href='http://bl.ocks.org/espinielli/b6e2f37814a19dc461f8'>Cahill-Keyes blues</a>